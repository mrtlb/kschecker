#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <errno.h>
#include "kscheck.h"

int kscheck(FILE *logFile, char *statusFileName, char *appName)
{
    char buff[255];
    FILE *stats = fopen("/proc/net/sockstat", "r");
    if (stats == NULL)
    {
        fprintf(logFile, "fopen(): %s", strerror(errno));
        return 1;
    }

    time_t now = time(&now);
    struct tm *now_info = localtime(&now);
    char now_string[30];
    strftime(now_string, 30, "%Y-%m-%d %H:%M:%S", now_info);
    int sockets = 0;

    //Sockets
    if (fgets(buff, 255, stats) != NULL)
    {
        char *last = strrchr(buff, ' ');
        sockets = strtol(++last, (char **)NULL, 10);
    }

    fprintf(logFile,
            "{\"app\":\"%s\",\"ts\":\"%s\",\"logger\":\"kschecker\",\"level\":\"INFO\",\"socket_count\":\"%d\",\"msg\":\"Available sockets : %d\"}\n",
            appName, now_string, sockets, sockets);

    if (statusFileName != NULL)
    {
        FILE *statusFile = NULL;
        if (makefile(logFile, &statusFile, statusFileName) > 0)
            return 1;

        fprintf(statusFile, "{\"ts\":\"%s\",\"socket_count\":\"%d\"}", now_string, sockets);
        if (statusFile != NULL)
        {
            fflush(statusFile);
            fclose(statusFile);
        }
    }
    return 0;
}

int makefile(FILE *err, FILE **file, char *fileName)
{
    *file = fopen(fileName, "w");
    if (*file == NULL)
    {
        fprintf(err, "fopen(%s): %s", fileName, strerror(errno));
        file = NULL;
        return 1;
    }
    return 0;
}