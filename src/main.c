#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include "main.h"
#include "kscheck.h"

int main(int argc, char *argv[])
{
    FILE *logFile = stdout;
    FILE *statusFile = NULL;
    int result = 0;

    bool isConsoleMode = false;

    char *logFileName = NULL;
    char *statusFileName = NULL;
    char *appName = "UNKNOWN";

    int pollingInterval = 5;

    int opt;
    while ((opt = getopt(argc, argv, "ca:t:l:s:")) != -1)
    {
        switch (opt)
        {
        case 'c':
            printf("Invoked in Console mode.\n");
            isConsoleMode = true;
            break;
        case 'l':
            printf("Logging to:\t%s\n", optarg);
            logFileName = optarg;
            break;
        case 's':
            printf("Status to:\t%s\n", optarg);
            statusFileName = optarg;
            break;
        case 'a':
            printf("AppName:\t%s\n", optarg);
            appName = optarg;
            break;            
        case 't':
            pollingInterval = strtol(optarg, (char **)NULL, 10);
            if (pollingInterval == 0)
                pollingInterval = 5;
            printf("Polling Interval:\t%d\n", pollingInterval);
            break;
        default:
            displayUsage(argv[0]);
        }
    }

    if (!isConsoleMode && logFileName == NULL && statusFileName == NULL)
    {
        printf("Nothing to do!  Pick something...\n");
        displayUsage(argv[0]);
    }

    if (!isConsoleMode)
    {
        // Make it a daemon
        result = makedaemon();
        logFile = NULL;
    }

    if (logFileName != NULL)
    {
        if (makefile(stderr, &logFile, logFileName) > 0)
            exit(1);
    }

    while (1 == 1)
    {
        //truncate file periodically
        if (logFile != stdout && ftell(logFile) > 1024*1024){
            fclose(logFile);
            makefile(stderr, &logFile, logFileName);
        } 
        
        kscheck(logFile, statusFileName, appName);
        fflush(logFile);
        sleep(pollingInterval);
    }
}

void displayUsage(char *programName)
{
    fprintf(stderr, "Usage: \t%s [-ca] [-l logfile] [-s statusfile] [-t seconds]\n\n"
                    "Flags:\n"
                    "\t-c\tLaunch in console mode (not a daemon) and use STDOUT for logging.\n"
                    "\t-l\tOutput logs to a file (logfile)\n"
                    "\t-s\tOutput a formatted JSON status file (statusfile)\n"
                    "\t-a\tapp name to log (default = UNKNOWN)\n"
                    "\t-t\tPolling interval in seconds (default 5)\n",
            programName);
    exit(EXIT_FAILURE);
}

int makedaemon()
{
    pid_t process_id = 0;
    pid_t sid = 0;

    // Create child process
    process_id = fork();
    // Indication of fork() failure
    if (process_id < 0)
    {
        printf("fork failed!\n");
        // Return failure in exit status
        exit(1);
    }

    // PARENT PROCESS. Need to kill it.
    if (process_id > 0)
    {
        printf("process_id of child process %d \n", process_id);
        // return success in exit status
        exit(0);
    }
    //unmask the file mode
    umask(0);
    //set new session
    sid = setsid();
    if (sid < 0)
    {
        // Return failure
        exit(1);
    }

    // Change the current working directory to root.
    //chdir("/");
    // Close stdin. stdout and stderr
    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);
    return (0);
}
