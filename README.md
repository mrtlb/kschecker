# kscheck #



### C utility to monitor pod state ###

* This application will eventually be a daemon in our Alpine pods.  It will monitor for certain conditions that are easier to detect natively than in Java (e.g. port exhaustion).
* When running and logging, consider streaming the logfile to stdout through a symlink.
  * https://worp.io/docker-symlink-your-log-files-to-stdout-and-stderr/
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Tested using Visual Studio Code with:
  * C/C++ Intellisense
  * Docker
  * brew install check (if you want intellisense to find check.h)
  * Makefile Tools
* `./buildAndRun.sh` will create the Docker container, build and run the test suite.
* `./buildAndRun.sh sh` will create and enter the build container without building or exiting.
  * **NOTE:**  The container and image will automatically clean up when you exit.

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact